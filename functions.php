<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

/* Import the parent styles */
function my_theme_enqueue_styles() {

    $parent_style = 'enfold'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

/*  My custom stuff starts here */

add_filter( 'gform_get_form_filter', 'shortcode_unautop', 11 );
add_filter( 'gform_get_form_filter', 'do_shortcode', 11 );

//MY FUNCTIONS

function wc_custom_subscription_price_string( $pricestring ) {
	// Applies only to the shopping cart subtotals
	// SUBTOTAL	€0,00 / month for 3 months
	//RECURRING TOTAL	€0,00 / month for 3 months
	//First renewal: 2020-05-16

	// Expected result
	// to-do: investigate shopping cart values

	return $pricestring;
}

  function wc_custom_subscriptions_product_price_string( $pricestring ) {
	// Add this to be able to access the product variable
	global $product;
	
	  // Works on the product page and products for subscription gifts
	  // to-do: Check if we can make this work with the category
      if ( 2989 == $product->id OR 1799 == $product->id ) {
			$array = preg_split("/ /", $pricestring);
			/* 
				Original string == €0,00 / month for 3 months and a €74,97 sign-up fee
				$array contains this: 
				Array ( [0] => class="woocommerce-Price-amount 
				[2] => amount"> class="woocommerce-Price-currencySymbol">€0,00 
				[4] => class="subscription-details"> 
				[6] => / 
				[7] => month 
				[8] => for 
				[9] => 3 
				[10] => months 
				[11] => and 
				[12] => a 
				[13] => class="woocommerce-Price-amount 
				[15] => amount">
				[16] => class="woocommerce-Price-currencySymbol">€74,97 
				[17] => sign-up 
				[18] => fee )
			*/

			$array15 = preg_replace( '/class="woocommerce-Price-currencySymbol">/', 'One time payment of ', $array[16] );
			$subscription_string = $array15;

			return $subscription_string;
	  }
      else{ 
		// Not our products so do nothing
        return $pricestring;
	  }
	}
	
add_filter( 'woocommerce_subscriptions_product_price_string', 'wc_custom_subscriptions_product_price_string' );
add_filter( 'woocommerce_subscription_price_string', 'wc_custom_subscription_price_string' );

//testing text in shopping cart

function filter_cart_item_price( $price, $cart_item, $cart_item_key ) {
    if ( $cart_item[ 'data' ]->price == 0 ) {
		//$price = __( 'Testing cart PRICE' );
		//$price = 'Testing cart PRICE';
		$arrayPrice = preg_split("/ /", $price);
		$arrayPrice2 = preg_replace( '/class="woocommerce-Price-currencySymbol">/', 'One time payment of ', $arrayPrice[16] );
		$price = $arrayPrice2;
    }
    return $price;
}

function filter_cart_item_subtotal( $subtotal, $cart_item, $cart_item_key ) {
    if ( $cart_item[ 'data' ]->price == 0 ) {
		//$subtotal = __( 'Testing cart SUBTOTAL' );
		//$subtotal = 'Testing cart SUBTOTAL';
		$arraySubtotal = preg_split("/ /", $subtotal);
		$arraySubtotal2 = preg_replace( '/class="woocommerce-Price-currencySymbol">/', 'One time payment of ', $arraySubtotal[17] );
		$subtotal = $arraySubtotal2;
    }
    return $subtotal;
}
//search info about those filters

//number 10 is the priority: Used to specify the order in which the functions associated with a particular action are executed. 
//Lower numbers correspond with earlier execution, and functions with the same priority are executed in the order in which they were added to the action. Default value: 10

//number 3 means that you have 3 parameters in the function, $accepted_args The number of arguments the function accepts. Default value: 1
//https://developer.wordpress.org/reference/functions/add_filter/

add_filter( 'woocommerce_cart_item_price', 'filter_cart_item_price', 10, 3 );
add_filter( 'woocommerce_cart_item_subtotal', 'filter_cart_item_subtotal', 10, 3 );

/* investigate first renewal
Filter: 'woocommerce_subscriptions_product_first_renewal_payment_time'

Parameters:
$first_renewal_timestamp A unix timestamp representing the date/time in the future in a user specified timezone.
$product_id The ID of the product to which the $first_renewal_timestamp value relates.
$from_date_param A MySQL formatted date/time string from which to calculate the date, or empty (default), which will use today’s date/time. Should be in UTC timezone.
$timezone The timezone for the returned date, either ‘site’ for the site’s timezone, or ‘gmt’. Default, ‘site’.

Description:
Change when a given product’s first renewal date should be scheduled to occur. This filter can be used to add custom lengths to the initial period of a subscription
for example, for synchronizing payments to a date not possible with the built-in synchronization feature.
The default value is based on the subscription product’s billing period and interval. However, it can be set to any valid date in the future.

*/